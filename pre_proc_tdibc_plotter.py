"""
Step 3: Plotting the results
------------------------------------------------------------------------------------------

Plots the target reflection coefficient (the one we want to model) versus the fitted reflection coefficient (the one imposed by TDIBC).

Step 3 plots:
    - The reflection coefficient in linear scale (real part, imaginary part, phase and modulus)
    - The reflection coefficient in semilogx scale (real part, imaginary part, phase and modulus)
    - The sum of error in semilogy scale (error on real part, imaginary part and modulus)

-----------------------------------------------------------------
"""

import multiprocessing
import os

import matplotlib.pyplot as plt
import numpy as np

import pre_proc_tdibc_fit as fitter
import pre_proc_tdibc_parameters as in_params


def plot_two_r_coeffs(data_tpl, path_tpl, label_tpl):
    """
    Plots the real part, imaginary part, modulus and phase
    of the TDIBC model (fit result) vs the target_r data.

    data_tpl : tuple of numpy arrays (real, complex, complex)
        frequency, target reflection coeff and fit result
    path_tpl : tuple of string
        path to output the plots
    label_tpl : tuple of string
        tuple of labels

    return : `None`
    """

    # Unpack data
    freq, r_coeff, model = data_tpl
    out_path, out_file = path_tpl
    r_coeff_label, model_label = label_tpl

    if out_path.endswith('/'):
        out_path = out_path[:-1]

    fitter.NpzReader.check_create_dir(out_path + '/Semilogx')
    fitter.NpzReader.check_create_dir(out_path + '/Linear')

    # ---------------------------------------------------------
    # Semilogx
    fig, axes = plt.subplots(2, 2, figsize=(12, 8))

    # Plot 1
    axes[0, 0].semilogx(freq, np.abs(r_coeff), label="%s" % r_coeff_label)
    axes[0, 1].semilogx(freq, np.angle(r_coeff) / np.pi)
    axes[1, 0].semilogx(freq, np.real(r_coeff))
    axes[1, 1].semilogx(freq, np.imag(r_coeff))

    # Plot 2
    axes[0, 0].semilogx(freq, np.abs(model), '--', label="%s" % model_label)
    axes[0, 1].semilogx(freq, np.angle(model) / np.pi, '--')
    axes[1, 1].semilogx(freq, np.imag(model), '--')
    axes[1, 0].semilogx(freq, np.real(model), '--')

    # Labels
    axes[0, 0].set_ylabel("Modulus [-]")
    axes[0, 1].set_ylabel("$\pi \\times$ Phase [-]")
    axes[1, 0].set_xlabel("Frequency [Hz]")
    axes[1, 0].set_ylabel("Real part [-]")
    axes[1, 1].set_xlabel("Frequency [Hz]")
    axes[1, 1].set_ylabel("Imaginary part [-]")

    # Legend
    axes[0, 0].legend()

    plt.tight_layout()
    plt.savefig(out_path + '/Semilogx/' + out_file + "_semilogx.pdf", dpi=400)
    plt.close()
    # ---------------------------------------------------------

    # ---------------------------------------------------------
    # Linear
    fig, axes = plt.subplots(2, 2, figsize=(12, 8))

    # Plot 1
    axes[0, 0].plot(freq, np.abs(r_coeff), label="%s" % r_coeff_label)
    axes[0, 1].plot(freq, np.angle(r_coeff) / np.pi)
    axes[1, 0].plot(freq, np.real(r_coeff))
    axes[1, 1].plot(freq, np.imag(r_coeff))

    # Plot 2
    axes[0, 0].plot(freq, np.abs(model), '--', label="%s" % model_label)
    axes[0, 1].plot(freq, np.angle(model) / np.pi, '--')
    axes[1, 1].plot(freq, np.imag(model), '--')
    axes[1, 0].plot(freq, np.real(model), '--')

    # Labels
    axes[0, 0].set_ylabel("Modulus [-]")
    axes[0, 1].set_ylabel("$\pi \\times$ Phase [-]")
    axes[1, 0].set_xlabel("Frequency [Hz]")
    axes[1, 0].set_ylabel("Real part [-]")
    axes[1, 1].set_xlabel("Frequency [Hz]")
    axes[1, 1].set_ylabel("Imaginary part [-]")

    # Legend
    axes[0, 0].legend()

    # Output the figure
    plt.tight_layout()
    plt.savefig(out_path + '/Linear/' + out_file + "_xLinear.pdf", dpi=400)
    plt.close()
    # ---------------------------------------------------------


def plot_error(n0_current, out_path, out_file, err_r, err_i, err_mod):
    """
    Plots all of the errors in semilogy plots:
        - logy --> Errors
        - x    --> Iterations

    n0_current : `int`
        Number of poles considered

    out_path : `str`
        Output path

    out_file : `str`
        Output file name

    err_r : `numpy.array(dtype=np.float64)'
        Sum of point-to-point distance on real part

    err_i : `numpy.array(dtype=np.float64)'
        Sum of point-to-point distance on imaginary part

    err_mod : `numpy.array(dtype=np.float64)'
        Sum of point-to-point distance on modulus

    return : `None`
    """
    # Logy
    plt.close()
    plt.figure()
    plt.semilogy(1 + np.arange(n0_current), err_r[0:n0_current], '--o', lw=1, label='Real')
    plt.semilogy(1 + np.arange(n0_current), err_i[0:n0_current], ':+', lw=1, label='Imaginary')
    plt.semilogy(1 + np.arange(n0_current), err_mod[0:n0_current], '-.x', lw=1, label='Modulus')
    plt.xlabel("Iterations [-]")
    plt.ylabel("Sum of point-to-point Error [-]")
    plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig(out_path + '/Error/' + out_file + "_error.pdf", dpi=400)
    plt.close()


def plotting_step():
    """
    Prepare the plotting using all the CPU available.

    return : `None`
    """
    # Get path of results (.npz)
    path = fitter.IterativeFit.get_out_fit_npz_path()

    # Filter potential 'dot files' and select only ".npz" files
    npz_files = [cur_file for cur_file in os.listdir(path) if not cur_file.startswith('.') if ".npz" in cur_file]
    check_file = in_params.OUT_PATH

    if check_file.endswith('/'):
        check_file = check_file[:-1]

    check_file += '/03_Plots_Result/Linear/'

    npz_files_to_plot = []

    # Select the .npz files that are not already plotted
    for filename in npz_files:
        print "\nfilename: %s" % filename
        file_to_check = check_file + filename.replace(".npz", "_xLinear.pdf")

        if os.path.isfile(file_to_check):
            print "\t\t  Data already plotted. Doing nothing..."
        else:
            print "\t\t  Plotting..."
            npz_files_to_plot.append(filename)

    return path, npz_files_to_plot, filename


def para_plot(args):
    """
    Used to launch parallel plots.

    args: `tuple`
        val1 = path `str`, val2 = filename `str`

    return : `None`
    """
    path, filename = args
    fitter.FitPlotter(path, filename)


if __name__ == "__main__":
    path, npz_files_to_plot, filename = plotting_step()

    # Multicore plot
    pool = multiprocessing.Pool()
    file_to_treat = zip([path] * len(npz_files_to_plot), npz_files_to_plot)
    pool.map(para_plot, file_to_treat)

    # Error plot
    fitter.FitPlotter(path, filename, error_only=True)
