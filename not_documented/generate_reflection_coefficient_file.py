"""
!!!
!!! WE USE THE EXP(-iwt) CONVENTION !!!
!!!
"""

import os

import matplotlib.pyplot as plt
import numpy as np

os.system("rm -f debug_figs/*pdf")

NscbcMode = False
rMinusOneMode = True


# import IO as wr


def export_ascii(text_file_name=None, dir_path='.', data_list=None, data_name_str=None):
    filename = dir_path + '/' + text_file_name + '.ascii'
    np.savetxt(filename, np.column_stack(data_list), header=data_name_str)


def reflection_coefficient_nscbc(frequency=None, k_relax=None):
    omega = 2. * np.pi * frequency
    return -1 / (1 - 1j * (2 * omega / k_relax))


def r_minus_one(frequency=0.):
    return (-1. + 1j * 0.) * np.ones_like(frequency)


if __name__ == "__main__":
    # Use proper matplotlib styling
    plt.style.use('matplotlib_styles/style_Q_Douasbin.mplstyle')

    # freq = np.logspace(0.0, 5, num=400, base=10.0)  # `num` values from 10^0 to 10^5
    freq = np.linspace(1., 5500., num=200)  # `num` values from 10^0 to 10^5

    if NscbcMode:
        r_coeff_out = reflection_coefficient_nscbc(frequency=freq, k_relax=1000.)
        # plotter.plot_single_r_coeff(freq, r_coeff=r_coeff_out, filename='debug_figs/r_coeff_nscbc')
    elif rMinusOneMode:
        r_coeff_out = r_minus_one(frequency=freq)
        # plotter.plot_single_r_coeff(freq, r_coeff=r_coeff_out, filename='debug_figs/r_coeff_minus_one')

    export_ascii(text_file_name='my_reflection_coefficient',
                 data_list=[freq, r_coeff_out.real, r_coeff_out.imag],
                 data_name_str='freq\tr_nscbc.real\tr_nscbc.imag')
