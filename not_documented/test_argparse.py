import argparse

desc_str = "Computes the p_k and mu_k needed for the TDIBC model.\n(TDIBC: Time Domain Impedance Boundary Condition)"
usage_str = 'python2 my_script.py --stepX (see below)'

parser = argparse.ArgumentParser(description=desc_str,usage=usage_str)

help_step1 = 'read reflection coefficient file .ascii), apply filter and/or time delay, plot the results and create numpy binary file (.npz) with the correct data for the fit'
help_step2 = 'iterative least-square fit. The results of each iteration is stored in a numpy binary file (.npz)'
help_step3 = 'plot the fit results (.pdf): every iteration + logx error plot'

parser.add_argument('-s1', '--step1', help=help_step1, action="store_true")
parser.add_argument('-s2', '--step2', help=help_step2, action="store_true")
parser.add_argument('-s3', '--step3', help=help_step3, action="store_true")

#parser.add_argument("-v", "--verbosity", help="increase output verbosity", action="store_true")
#parser.add_argument("-b", "--berbosity", help="increase output verbosity", action="store_false")

args = parser.parse_args()
#print args

if args.step1:
  print "\ndo step1"
elif args.step2:
  print "\ndo step2"
elif args.step3:
  print "\ndo step3"
else:
  print "\nYou asked for no step. Doing nothing...\n\nPlease check how to use this script running the command: 'python2 my_script.py --help'"
