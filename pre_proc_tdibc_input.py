"""
Step 1: Prepare the reflection coefficient date for the fit.
---------------------------------------------------------------

Loads the `.ascii` file corresponding to the reflection coefficient.
The `.ascii` file must be under the 3 column format:
    - First column: frequency array
    - Second column: real part of reflection coefficicent
    - Third column: imaginary part of reflection coefficient

Then applies treatments to it, if needed:
    - low-pass filter (defined in the `pre_proc_tdibc_paramaters.py` file)
    - add pure time delay (defined in the `pre_proc_tdibc_paramaters.py` file)

Finally, it outputs a `.npz` file that will be used in **Step 2**

-----------------------------------------------------------------
"""

import os

import matplotlib.pyplot as plt
import numpy as np

import pre_proc_tdibc_fit as fitter
import pre_proc_tdibc_parameters as in_params
import pre_proc_tdibc_plotter as plotter


class InputRCoeff(object):
    """
    Loads the `.ascii` file corresponding to the input_r reflection coefficient.
    The `.ascii` file must be under the 3 column format:
        - First column: frequency array
        - Second column: real part of reflection coefficicent
        - Third column: imaginary part of reflection coefficient
    """

    def __init__(self, out_path, file_name, skiprows=None):
        self.outpath = out_path
        self.filename = file_name
        self.skip_rows = skiprows
        self.read_ascii()
        self.save_params()

    def __str__(self):
        """
        Prints class information while running the command `print Obj`
        where Obj is an instance of the class InputReflectionCoeff.

        return : `None`
        """
        name = self.__class__.__name__
        out = "This an instance of class %s.\n" % name
        out += "It contains the following data:\n"
        attrs = vars(self)
        out += ',\n'.join("\t%s" % item for item in attrs.keys())
        return out

    def read_ascii(self):
        """
        Reads the `.ascii` file specified in the `pre_proc_tdibc_paramaters.py` file.

        return : `None`
        """
        data_txt = np.loadtxt(self.filename, skiprows=self.skip_rows)
        self.freq = data_txt[:, 0]
        self.r_coeff = data_txt[:, 1] - 1j * data_txt[:, 2]

    @staticmethod
    def get_saved_params_path():
        """
        Get path where the input_r parameter file is saved (to keep track)
        
        return : `None`
        """
        # Build and check output path
        out_path = in_params.OUT_PATH

        if out_path.endswith('/'):
            out_path = out_path[:-1]
        out_path += "/00_InputParameters/"

        return out_path

    def save_params(self):
        """
        Save input_r parameters file
        """
        from shutil import copyfile

        # Get path and check if directory exists
        out_path = self.get_saved_params_path()
        fitter.IterativeFit.check_create_dir(out_path)

        # Copy parameter file in order to keep track of what was done
        copyfile("pre_proc_tdibc_parameters.py", out_path + "pre_proc_tdibc_parameters.py")


class TargetRCoeff(InputRCoeff):
    """
    Class that takes the data from the input_r `.ascii` file and applies treatments:
        - low-pass filter (defined in the `pre_proc_tdibc_paramaters.py` file)
        - add pure time delay (defined in the `pre_proc_tdibc_paramaters.py` file)
    """

    def __init__(self, out_path, file_name, skiprows=None):
        InputRCoeff.__init__(self,
                             out_path=out_path,
                             file_name=file_name,
                             skiprows=skiprows)
        self.r_coeff = self.add_time_delay()
        self.filter_r()
        self.dump_npz()

    def add_time_delay(self):
        """
        Adds a pure time delay (defined in the `pre_proc_tdibc_paramaters.py` file)

        return : `None`
        """
        tau = in_params.PURE_TIME_DELAY
        return self.r_coeff * np.exp(-1j * 2. * np.pi * self.freq * tau)

    def filter_r(self):
        """
        Apply a low-pass filter (defined in the `pre_proc_tdibc_paramaters.py` file)

        return : `None`
        """

        if in_params.FILTER_TYPE == "tanh":
            def env_tanh():
                """
                Hyperbolic tangent envelope. This is used to low-pass filter the reflection coefficient.
                Returns
                -------
                res : numpy.array(dtype=np.float)
                      envelope function using hyperbolic tangent
                """
                res = - np.tanh((self.freq - (in_params.FILTER_F_CUTOFF
                                              + in_params.FILTER_TRANSITION_WIDTH))
                                / (in_params.FILTER_TRANSITION_WIDTH / 3.))
                return res

            self.r_coeff = 0.5 * (1. + env_tanh()) * self.r_coeff
            plt.plot(self.freq, self.r_coeff)

        elif in_params.FILTER_TYPE == "none":
            pass
        else:
            raise NameError(
                "Filter '%s' is non implemented in class '%s',"
                " Only possible options are 'tanh' and 'none'" % (
                    in_params.FILTER_TYPE, self.__class__.__name__))

    def dump_npz(self):
        """
        Writes the reflection coefficient after treatment (fitler + delay) as a `.npz` file.
        This file will be read by the IterativeFit class.
        return : `None`
        """
        out_dir = self.outpath + '/npz'
        fitter.IterativeFit.check_create_dir(out_dir)
        np.savez(out_dir + '/' + self.filename.replace(".ascii", ".npz"),
                 freq=self.freq, r_coeff=self.r_coeff)


if __name__ == "__main__":
    # Use proper matplotlib styling
    plt.style.use('matplotlib_styles/style_Q_Douasbin.mplstyle')

    # Build and check output path
    output_path = in_params.OUT_PATH

    if output_path.endswith('/'):
        output_path = output_path[:-1]
    output_path += '/01_Target_reflection_coeff'

    os.system("rm -rf %s" % output_path)
    fitter.IterativeFit.check_create_dir(output_path)

    # Get output file name
    out_file = in_params.REFLECTION_COEFFICIENT_FILE.replace(".ascii", "")

    input_r = InputRCoeff(output_path, in_params.REFLECTION_COEFFICIENT_FILE,
                          skiprows=1)

    target_r = TargetRCoeff(output_path, in_params.REFLECTION_COEFFICIENT_FILE,
                            skiprows=1)

    # Plot
    data_tpl = input_r.freq, input_r.r_coeff, target_r.r_coeff
    path_tpl = output_path + '/pdf/' , 'asciiInput_vs_FilterDelayedTarget'
    label_tpl = "$R_{Input}$", "$R_{Target}$"
    plotter.plot_two_r_coeffs(data_tpl, path_tpl, label_tpl)
