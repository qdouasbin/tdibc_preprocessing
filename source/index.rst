.. TDIBC Tool documentation master file, created by
   sphinx-quickstart on Wed Sep 27 16:58:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TDIBC Tool's documentation!
========================================

The TDIBC Tool is composed of 5 steps.
They are described below.

Step 0: Filling the input parameters
------------------------------------------------------------------------------------------

The parameters are:
  - REFLECTION_COEFFICIENT_FILE: the file containing the reflection coefficient.
  - FILTER_TYPE: type of filter used (only "none" and "tanh" options available)
  - FILTER_F_CUTOFF: the lowest frequency impacted by the low-pass filter
  - FILTER_TRANSITION_WIDTH: the frequency range taken by the window to go from 1 to 0.
  - PURE_TIME_DELAY: time for a acoustic wave to travel back and forth a distance L at a constant speed of sounnd c0.
  - MAX_NB_POLES: number of conjugate pairs of pole and residues taken into account in order to build the model
  - OUT_PATH: path of the output directory


Loads the `.ascii` file corresponding to the reflection coefficient.
The `.ascii` file must be under the 3 column format:
  - First column: frequency array
  - Second column: real part of reflection coefficicent
  - Third column: imaginary part of reflection coefficient
------------------------------------------------------------------------------------------

Step 1: Prepare the reflection coefficient date for the fit.
---------------------------------------------------------------

Loads the `.ascii` file corresponding to the reflection coefficient.
The `.ascii` file must be under the 3 column format:
    - First column: frequency array
    - Second column: real part of reflection coefficicent
    - Third column: imaginary part of reflection coefficient

Then applies treatments to it, if needed:
  - low-pass filter (defined in the `pre_proc_tdibc_paramaters.py` file)
  - add pure time delay (defined in the `pre_proc_tdibc_paramaters.py` file)

Finally, it outputs a `.npz` file that will be used in **Step 2**

-----------------------------------------------------------------

Step 2: Performs an Iterative fit to determine the constants needed by the TDIBC model.
------------------------------------------------------------------------------------------

Step 2 consists in:
  - Read the `.npz` file produced in step 1 (binary numpy data)
  - Setup the fitting parameters
  - Performs the iterative fit. At each iteration:
    - a degree of freedom is added
    - smart initial guesses are used (**critical for time delayed reflection coefficients!!!**)
    - a Least-Square Algorithm is used to optimize the model
    - the results the iteration are saved in an `.npz` file per iteration that will be used in **Step 3** to plot the results.

-----------------------------------------------------------------

Step 3: Plotting the results
------------------------------------------------------------------------------------------

Plots the target reflection coefficient (the one we want to model) versus the fitted reflection coefficient (the one imposed by TDIBC).

Step 3 plots:
  - The reflection coefficient in linear scale (real part, imaginary part, phase and modulus)
  - The reflection coefficient in semilogx scale (real part, imaginary part, phase and modulus)
  - The sum of error in semilogy scale (error on real part, imaginary part and modulus)


Step 4: Add TDIBC model to SolutBound
------------------------------------------------------------------------------------------
To be done in separate tool ?


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
