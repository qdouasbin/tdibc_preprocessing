"""
Step 2: Performs an Iterative fit to determine the constants needed by the TDIBC model.
------------------------------------------------------------------------------------------

Step 2 consists in:
    - Read the `.npz` file produced in step 1 (binary numpy data)
    - Setup the fitting parameters
    - Performs the iterative fit. At each iteration:
        - a degree of freedom is added
        - smart initial guesses are used (**critical for time delayed reflection coefficients!!!**)
        - a Least-Square Algorithm is used to optimize the model
        - the results the iteration are saved in an `.npz` file per iteration that will be used in **Step 3** to plot the results.

-----------------------------------------------------------------
"""

import os

import lmfit
import matplotlib.pyplot as plt
import numpy as np

import pre_proc_tdibc_input as tdibc_step1
import pre_proc_tdibc_parameters as in_params
import pre_proc_tdibc_plotter as tdibc_plotter

plt.style.use('matplotlib_styles/style_Q_Douasbin.mplstyle')


class NpzReader(object):
    """
    Class that takes a `.npz` and initializes the attributes
    of an NpzReader object with the `npz.__dict__.keys()`
    """

    def __init__(self, path_to_npz):
        self.path_to_file = path_to_npz
        self.data = np.load(path_to_npz)
        self.get_npz_data(self.data)

    def get_npz_data(self, *args, **kwargs):
        """
        Unpack npz to attributes.

        args: [numpy.arrays]
            dictionary's values
        kwargs: [numpy.arrays]
            dictinary's keys
        """
        for my_dict in args:
            for key in my_dict:
                setattr(self, key, my_dict[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    @staticmethod
    def check_create_dir(out_dir):
        """
        Checks if directory exists. Creates it otherwise.

        out_dir : str
            Path of the directory to be checked.
        """
        if not os.path.exists(out_dir):
            os.system("mkdir -p %s" % out_dir)

    def __str__(self):
        """
        Prints the attributes of NpzReader object while doing the command `print NpzReader`
        return : `None`
        """
        name = self.__class__.__name__
        out = "This an instance of class %s.\n" % name
        out += "It contains the following data:\n"
        for k in self.data.keys():
            out += "\t%s\n" % k
        return out


class IterativeFit(NpzReader):
    """
    Class containig all the methods necessary for the fit
    """

    def __init__(self):
        # Load `.npz` data
        try:
            NpzReader.__init__(self, self.get_input_npz_path())
        except NameError:
            raise ("file %s no found. Redo step 1"
                   % in_params.REFLECTION_COEFFICIENT_FILE.replace(".ascii", ".npz"))

        # Check modeling quantity chosen in pre_proc_tdibc_parameters.py
        if not in_params.MODELING_QUANTITY == "reflection":
            if not in_params.MODELING_QUANTITY == "softness":
                raise NameError(
                    '\n\n\tMODELING_QUANTITY must be either "reflection" or "softness"\n\t'
                    'Please, correct the file "pre_proc_tdibc_parameters.py"')

        self.check_change_params()

        # Frequencies
        self.omega = 2. * np.pi * self.freq
        self.s_laplace = 1j * self.omega

        # Maximum number of poles considered
        self.n0_max = in_params.MAX_NB_POLES

        # Selecting "reflection coefficient" of "wall softness" mode
        self.modeling_quantity = in_params.MODELING_QUANTITY

        # current number of poles considered
        self.n0_current = 1
        self.k_pole = 0

        # List of parameters (Init to the correct size at once, faster that 'append' operation)
        self.idx_arr = np.zeros(self.n0_max, dtype=np.int)
        self.omega_0_k = np.zeros(self.n0_max)
        self.a_array = np.zeros(self.n0_max)
        self.c_array = np.ones(self.n0_max)
        self.d_array = np.ones(self.n0_max)

        # Error arrays
        self.max_err_r = np.zeros(self.n0_max)
        self.max_err_i = np.zeros(self.n0_max)
        self.max_err_mod = np.zeros(self.n0_max)

        # Clean output directory
        os.system("rm -rf %s" % self.get_out_fit_npz_path())
        os.system("rm -rf %s" % FitPlotter.get_plot_path())

        # attributes to be outputed in `.npz`
        self.out_data_list = ['a_array', 'c_array', 'd_array', 'n0_current',
                              'n0_max', 'freq', 'fit_result', 'r_coeff',
                              'max_err_r', 'max_err_i', 'max_err_mod']

        # get list of the names of the fit parameters
        self.init_list_names_params()

        # Fit data
        self.fit_result = None
        self.fit_params = None

        # Count number of peaks due to time delay
        self.n_delay_peaks = None
        self.freq_range_one_peak = None

        # Start the fitting procedure
        self.fit_loop()

    def check_change_params(self):
        """
        Check if the file `pre_proc_tdibc_parameters.py` is unchanged.
        If it has changed, we need to restart from step1.
        
        return : `None`
        """
        import filecmp
        filename_1 = 'pre_proc_tdibc_parameters.py'
        filename_2 = tdibc_step1.InputRCoeff.get_saved_params_path() + filename_1
        filename_2 = tdibc_step1.InputRCoeff.get_saved_params_path() + filename_1
        if not filecmp.cmp(filename_1, filename_2):
            raise NameError('\n\n\tThe File %s has changed.\n\t'
                            'Please, restart from step1' % filename_1)

    def get_input_npz_path(self):
        """
        Find the path to `.npz` input_r data
        
        return : `None`
        """
        # Build and check output path
        npz_path = in_params.OUT_PATH

        if npz_path.endswith('/'):
            npz_path = npz_path[:-1]
        npz_path += '/01_Target_reflection_coeff/npz/'

        out_file = in_params.REFLECTION_COEFFICIENT_FILE.replace(".ascii", ".npz")
        return npz_path + out_file

    def fit_loop(self):
        """
        Loop of the iterative fit
        
        return : `None`
        """
        # Initialize the fit parameters
        self.init_fit()
        self.core_fit()
        self.increment_nb_pbf()

        # Main loop for fit
        for n_current in xrange(0, self.n0_max - 1):
            self.add_pbf()
            self.core_fit()
            self.increment_nb_pbf()

    def init_fit(self):
        """
        Initialize all parameters needed for the first iteration of the fit
        
        return : `None`
        """

        # Current pole index (n0_current - 1)
        k_pole = self.k_pole

        if in_params.FILTER_F_CUTOFF < np.amax(self.freq):
            high_freq = in_params.FILTER_F_CUTOFF
        else:
            high_freq = np.amax(self.freq)

        # Number of extrema introduced by the time delay
        # This is used to initialize several poles at once to converge faster
        self.n_delay_peaks = 2. * in_params.PURE_TIME_DELAY \
                             * (high_freq - np.amin(self.freq)) + 1
        self.freq_range_one_peak = self.n_delay_peaks / (
            2. * in_params.PURE_TIME_DELAY * (high_freq - np.amin(self.freq)))

        # Init lmfit parameters
        self.fit_params = lmfit.Parameters()

        # Init the fit result array
        self.fit_result = np.zeros_like(self.freq, dtype=np.complex64)

        # Identify the resonant angular frequency of pole 1
        real_r_coeff = np.real(self.r_coeff)
        self.idx_arr[k_pole] = np.abs(real_r_coeff).argmax()

        # Initiliaze Pole Base Function parameters for pole 1
        self.omega_0_k[k_pole] = 2. * np.pi * self.freq[self.idx_arr[k_pole]]
        self.init_ck_and_dk()
        self.init_ak()

        # Adding and initialize parameters to the fit
        self.fit_params.add(self.a_array_name[k_pole], value=self.a_array[k_pole])
        self.fit_params.add(self.c_array_name[k_pole], max=0., value=self.c_array[k_pole])
        self.fit_params.add(self.d_array_name[k_pole], value=self.d_array[k_pole])

    def core_fit(self):
        """
        Main fit function. Calls the optimization algorithm
        an update the properties of the IterativeFit object
        return : `None`
        """
        print "\nIteration %s out of %s" % (self.n0_current, self.n0_max)

        def tdibc_residual(lmfit_parameters):
            """
            Compute residue (array) from the current fitting parameters
            :param lmfit_parameters: [lmfit.object] Lmfit parameters
            (contains the value and the name of the parameters)
            return : 
            """
            # Get result of fit
            fit_res = self.get_fit_result(lmfit_parameters)

            if self.modeling_quantity=="softness":
                fit_res -= 1.

            # Compute real and Imaginary residuals
            residue_re = np.square(self.r_coeff.real - fit_res.real)
            residue_im = np.square(self.r_coeff.imag - fit_res.imag)

            # concatenate in order to have a residual array
            return np.sqrt(np.concatenate((residue_re, residue_im)))

        if self.n0_current < self.n_delay_peaks:
            # We target_r each 'delay peak' with new initial
            # guesses before startinbg the fit (speed up the convergence)
            pass
        else:
            out = lmfit.minimize(tdibc_residual, self.fit_params, method='powell')
            # out = lmfit.minimize(tdibc_residual, self.fit_params, method='cg')
            # out = lmfit.minimize(tdibc_residual, self.fit_params)
            self.fit_params = out.params

        self.fit_result = self.get_fit_result(self.fit_params)
        self.compute_errors()
        self.output_results_npz()
        self.write_out_ascii()
        self.update_parameters(self.fit_params)

    def increment_nb_pbf(self):
        """
        Increment the order of the TDIBC model: increment model order attributes
        
        return : `None`
        """
        self.n0_current += 1
        self.k_pole += 1

    def add_pbf(self):
        """
        Increment the order of the TDIBC model: add a PBF
        
        return : `None`
        """
        # Pole Index
        k_pole = self.k_pole

        # Identify the resonant angular frequency for pole number 'k_pole'
        self.idx_arr[k_pole] = np.square(np.real(self.r_coeff) - np.real(self.fit_result)).argmax()
        self.omega_0_k[k_pole] = 2. * np.pi * self.freq[self.idx_arr[k_pole]]

        # Get ck and dk for new pole
        self.init_ck_and_dk()

        # Get ak for new pole
        self.init_ak()

        # Adding and initialize parameters to the fit
        self.fit_params.add(self.a_array_name[k_pole], value=self.a_array[k_pole])
        self.fit_params.add(self.c_array_name[k_pole], max=0., value=self.c_array[k_pole])
        self.fit_params.add(self.d_array_name[k_pole], value=self.d_array[k_pole])

    def init_list_names_params(self):
        """
        Generates lists of names of fit parameters for a pole number k

        return : `None`
        """
        self.a_array_name = ['a' + str(i + 1) for i in xrange(self.n0_max)]
        self.c_array_name = ['c' + str(i + 1) for i in xrange(self.n0_max)]
        self.d_array_name = ['d' + str(i + 1) for i in xrange(self.n0_max)]

    def init_ck_and_dk(self, epsilon=0.15, alpha=0.1):
        """
        Finds the ck and dk for a given time delay.

        epsilon: `float`
            percentage of peak height (typically 1 to 5 percent).
        alpha: `float`
            used for tau==0., alpha links c_k_pole and d_k_pole in this case.

        return : `None`
        """
        k_pole = self.k_pole
        tau = in_params.PURE_TIME_DELAY

        omega0 = self.omega_0_k[k_pole]

        c_k_pole = - np.sqrt(omega0 ** 2. / (1 + alpha ** 2.))
        d_k_pole = alpha * c_k_pole

        if omega0 < 2. * np.pi * self.freq_range_one_peak:
            omega0 *= 100.

        if tau != 0.:
            f_range = np.pi / (2. * tau)
            big_eps = np.sqrt(epsilon / (1. - epsilon))
            c_k_pole = - np.abs(big_eps * ((omega0 * f_range + 0.5 * f_range) / (omega0 + f_range)))

            # Known relationship
            if (omega0 ** 2. - c_k_pole ** 2.) > 0.:
                d_k_pole = np.sqrt(omega0 ** 2. - c_k_pole ** 2.)
            else:
                d_k_pole = np.sqrt(c_k_pole ** 2. - omega0 ** 2.)

        self.c_array[k_pole] = c_k_pole
        self.d_array[k_pole] = d_k_pole

    def init_ak(self):
        """
        Initialize ak parameter for pole k.

        return : `None`
        """

        max_r_diff = self.r_coeff[self.idx_arr[self.k_pole]].real
        self.a_array[self.k_pole] = - self.c_array[self.k_pole] * max_r_diff

    def compute_errors(self):
        """
        Computes and store the sum of the point-to-point error
        for real part, imaginary part and modulus.
        
        return : `None`
        """
        self.max_err_r[self.k_pole] = np.sum(np.abs(np.real(self.r_coeff - self.fit_result)))
        self.max_err_i[self.k_pole] = np.sum(np.abs(np.imag(self.r_coeff - self.fit_result)))
        self.max_err_mod[self.k_pole] = np.sum(np.abs(self.r_coeff - self.fit_result))

    def update_parameters(self, out_minimize):
        """
        Stores the current values (at the end of fit iteration) into attributes

        out_minimize : `lmfit.object`
            output fit parameters
        
        return : `None`
        """
        # Get the values of the params
        a_arr = [out_minimize['a' + str(x + 1)].value for x in xrange(self.n0_current)]
        c_arr = [out_minimize['c' + str(x + 1)].value for x in xrange(self.n0_current)]
        d_arr = [out_minimize['d' + str(x + 1)].value for x in xrange(self.n0_current)]

        # Used for slicing
        n_lines = np.array(c_arr).shape[0]

        # Store results using numpy array slices
        self.a_array[0:n_lines] = np.array(a_arr)
        self.c_array[0:n_lines] = np.array(c_arr)
        self.d_array[0:n_lines] = np.array(d_arr)

    def get_fit_result(self, lmfit_params):
        """
        Computes results of the fit function

        lmfit_params: `lmfit.object`
            current fitting parameters
        
        return : `numpy.array`
            fit function result
        """
        s_laplace = np.array(self.s_laplace)
        n0 = self.n0_current

        a_arr = np.array([lmfit_params['a' + str(x + 1)].value for x in xrange(self.n0_current)])
        c_arr = np.array([lmfit_params['c' + str(x + 1)].value for x in xrange(self.n0_current)])
        d_arr = np.array([lmfit_params['d' + str(x + 1)].value for x in xrange(self.n0_current)])

        # Reinit to zero
        res = np.zeros_like(s_laplace)

        # todo: could be vectorized (function slowing the computation)
        for i in xrange(n0):
            num = (2. * a_arr[i] * s_laplace)
            denum = s_laplace ** 2. - 2. * c_arr[i] * s_laplace + c_arr[i] ** 2. + d_arr[i] ** 2.
            res += num / denum

        return res

    def output_results_npz(self):
        """
        Writes a numpy binary file (`.npz`) using the keys defined in `IterativeFit.out_data_list`.
        
        return : `None`
        """
        out_dict = {}
        class_dict = self.__dict__

        # Select data to be stored
        for name in self.out_data_list:
            out_dict[name] = class_dict[name]

        # Build and check path
        out_path = self.get_out_fit_npz_path()
        out_file = in_params.REFLECTION_COEFFICIENT_FILE.replace(".ascii", "")
        self.check_create_dir(out_path)

        # Save data
        np.savez_compressed(out_path + out_file + "_%04d.npz" % (self.n0_current), **out_dict)

    @staticmethod
    def get_out_fit_npz_path():
        """
        Get path output `.npz` of fit results.
        
        return : `str`
            output path for `.npz`
        """
        # Build and check path
        out_path = in_params.OUT_PATH

        if out_path.endswith('/'):
            out_path = out_path[:-1]

        out_path = in_params.OUT_PATH + '/02_Fit/'

        return out_path

    def write_out_ascii(self):
        """
        Writes a `.ascii` file containing the TDIBC model at a given iteration.
        This `.ascii` file will be used to add the TDIBC model to the `solutBound.h5`.

        return : `None`
        """
        a_arr = self.a_array[:self.n0_current]
        c_arr = self.c_array[:self.n0_current]
        d_arr = self.d_array[:self.n0_current]
        b_arr = - a_arr * c_arr / d_arr

        # out = ['%s\t\t\t! Number of poles\n' % self.n0_current]

        out = []

        out.append(self.n0_current)

        for i, (a, b, c, d) in enumerate(zip(a_arr, b_arr, c_arr, d_arr)):
            out.append(c)
            out.append(d)
            out.append(a)
            out.append(b)

        out_filename = 'tdibc_model_%s.txt' % self.n0_current
        out_path = self.get_out_fit_npz_path().replace("npz", "ascii")
        self.check_create_dir(out_path)

        np.savetxt(out_path + out_filename, out, fmt='%18.18e')  # use exponential notation


class FitPlotter(NpzReader):
    """
    Class used to plot the fit results.
    """

    def __init__(self, path_to_file, filename, error_only=False):
        """
        Class init from

        Parameters
        ----------
        path_to_file : `str`
            path of the directory where we can find the`.npz` file
        filename : `str`
            name of the file `.npz` file
        error_only : `bool`
            if ``True`` the error plot is generated for the current `.npz` file
        """
        NpzReader.__init__(self, path_to_file + filename)
        self.path_to_file = path_to_file
        self.filename = filename

        if error_only:
            print 'Plotting Error vs iterations...'
            self.plot_error()
        else:
            self.plot_from_npz()

    def plot_from_npz(self):
        """
        Plots the target_r vs TDIBC model reflection coefficients.
        Plotting real part, imaginary part, modulus and phase on separate subplots.
        Output: both semilogx and x linear plots.
        
        return : `None`
        """
        # Build and check path
        out_path = self.get_plot_path()
        # output_path +=  '/pdf'
        out_file = self.filename.replace(".npz", "")
        self.check_create_dir(out_path + '/Linear')
        self.check_create_dir(out_path + '/Semilogx')

        # Plot
        data_tpl = self.freq, self.r_coeff, self.fit_result
        path_tpl = out_path, out_file
        label_tpl = "$R_{Traget}$", "$R_{Fit}$"
        tdibc_plotter.plot_two_r_coeffs(data_tpl, path_tpl, label_tpl)

    def plot_error(self):
        """
        Plots the error on real part, imaginary part and modulus.
        Error plotted: sum of point-to-point error.
        
        return : `None`
        """
        # Build and check path
        out_path = self.get_plot_path()
        # out_path += '/pdf'
        out_file = self.filename.replace(".npz", "")
        self.check_create_dir(out_path + '/Error')

        # Plot
        tdibc_plotter.plot_error(self.n0_current, out_path, out_file,
                                 self.max_err_r, self.max_err_i, self.max_err_mod)

    @staticmethod
    def get_plot_path():
        """
        Path where the plots are generated.

        return : `None`
        """
        plot_path = in_params.OUT_PATH

        if plot_path.endswith('/'):
            plot_path = plot_path[:-1]

        plot_path += '/03_Plots_Result'
        return plot_path


if __name__ == "__main__":
    my_fit = IterativeFit()
