"""
Step 0: Filling the input parameters
------------------------------------------------------------------------------------------

The parameters are:
    - REFLECTION_COEFFICIENT_FILE: the file containing the reflection coefficient.
    - MODELING_QUANTITY: we can model the reflection coefficient (``$R(\omega)$``) or the softness (``$S(\omega)$``)
        - ``$S(\omega) = R(\omega) + 1$``
        - The reflection coefficient R is advised for:
            - Inlets
            - Outlets
            - Any time delayed reflection
        - The wall softness S is advised for:
            - Walls-like boundaries (perforated plates, acoustic liners, *etc.*)
    - FILTER_TYPE: type of filter used (only "none" and "tanh" options available)
    - FILTER_F_CUTOFF: the lowest frequency impacted by the low-pass filter
    - FILTER_TRANSITION_WIDTH: the frequency range taken by the window to go from 1 to 0.
    - PURE_TIME_DELAY: time for a acoustic wave to travel back and forth a distance L at a constant speed of sounnd c0.
    - MAX_NB_POLES: number of conjugate pairs of pole and residues taken into account in order to build the model
    - OUT_PATH: path of the output directory


Loads the `.ascii` file corresponding to the reflection coefficient.
The `.ascii` file must be under the 3 column format:
    - First column: frequency array
    - Second column: real part of reflection coefficicent
    - Third column: imaginary part of reflection coefficient
-----------------------------------------------------------------
"""


# Input file containing the reflection coefficient
# The .ascii file must be under the 3 column format:
#     - First column: frequency array
#     - Second column: real part of reflection coefficicent
#     - Third column: imaginary part of reflection coefficient
REFLECTION_COEFFICIENT_FILE = 'my_reflection_coefficient.ascii'

# Model reflection coefficient or softness ("reflection" or "softness")
# softness = reflection + 1
MODELING_QUANTITY = 'softness'
# MODELING_QUANTITY = 'reflection'

# Filter: choices --> "tanh", "none"
FILTER_TYPE = "none"

# Filter cut off frequency [Hz]
FILTER_F_CUTOFF = 10000.  # Start decay at this frequency

# Filter transition width [Hz]
FILTER_TRANSITION_WIDTH = 500.  # Stops decay at cutoff frequency + width

# Time Delay [s_laplace], different from zero if we model a pure time delay
PURE_TIME_DELAY = 0.5e-3

# Defining the maximum number of Pole Base Functions
MAX_NB_POLES = 15

# The name of the output directory
# It will be created if it does not already exists
OUT_PATH = "./my_output_folder/"
